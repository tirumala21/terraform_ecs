variable "key_name" {
  type        = string
  default     = "terraform"
  description = "The name for ssh key, used for aws_launch_configuration"
}

variable "cluster_name" {
  type        = string
  default     ="Cluster1"
  description = "The name of AWS ECS cluster"
}
